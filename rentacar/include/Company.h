//
// Created by Krzysztof Kosmala on 14.01.2018.
//

#ifndef PROJECT_COMPANY_H
#define PROJECT_COMPANY_H
#include <iostream>
#include <string>
#include <list>
#include "Employee.h"
#include "Template.h"
#include <boost/shared_ptr.hpp>
using namespace std;

class Company
{
private:
    string nameCompany;
    list<shared_ptr<Employee>> employees;
public:
    Company(string nameCompany);
    void addEmployee(shared_ptr<Employee> pranownik);
    void displayEmployees();
    void getTotalSalary();
};


#endif //PROJECT_COMPANY_H
