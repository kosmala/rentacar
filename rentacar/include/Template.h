//
// Created by Marcin Kwapisz on 2017-01-02.
//

#ifndef PROJECT_TEMPLATE_H_H
#define PROJECT_TEMPLATE_H_H
#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
template<typename T>
class Template
{
public:
    std::string toString(T value)
    {
        std::stringstream s;
        s << value;
        return s.str();
    }
};
#endif //PROJECT_TEMPLATE_H_H

