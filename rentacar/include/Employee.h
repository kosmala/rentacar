//
// Created by Krzysztof Kosmala on 14.01.2018.
//

#ifndef PROJECT_EMPLOYEE_H
#define PROJECT_EMPLOYEE_H
#include <iostream>
#include <string>
using namespace std;

class Employee
{
private:
    string name;
    double salary;
public:
    Employee(string name, double salary);
    void setName(string name);
    void setSalary(double salary);
    string getName();
    double getSalary();
};


#endif //PROJECT_EMPLOYEE_H
