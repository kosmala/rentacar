//
// Created by Krzysztof Kosmala on 14.01.2018.
//

#include "Employee.h"

Employee::Employee(string name, double salary) : name(name),salary(salary)
{

}

void Employee::setName(string name)
{
   this->name=name;
}

void Employee::setSalary(double salary)
{
    this->salary=salary;
}

string Employee::getName() {
    return this->name;
}

double Employee::getSalary() {
    return this->salary;
}
