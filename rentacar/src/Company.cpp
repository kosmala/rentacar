//
// Created by Krzysztof Kosmala on 14.01.2018.
//

#include "Company.h"

void Company::addEmployee( shared_ptr<Employee> pracownik)
{
    employees.push_back(pracownik);
}

Company::Company(string nameCompany) : nameCompany(nameCompany)
{

}

void Company::displayEmployees()
{
    for(auto it : employees)
    {
        cout<<"nazwa pracownika: "<<it->getName()<<" wynagrodzenie: "<<it->getSalary()<<endl;
    }
}

void Company::getTotalSalary()
{
    double zarobki=0;
    for(auto it : employees)
    {
        zarobki=zarobki+it->getSalary();
    }
    cout<<"Calkowity koszt: "<< zarobki;
}
