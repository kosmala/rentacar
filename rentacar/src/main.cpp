//
// Created by Marcin Kwapisz on 2017-01-02.
//

#include <iostream>
#include "Template.h"
#include "Employee.h"
#include "Company.h"


using namespace std;

int main()
{

    shared_ptr<Company> firma (new Company("Poli"));
    shared_ptr<Employee> empl  (new Employee("Krzysztof Kosmala",12000));
    firma->addEmployee(empl);
    empl = make_shared<Employee>("Janusz Kowalski",1500);
    firma->addEmployee(empl);
    empl = make_shared<Employee>("Adam Polak",2000);
    firma->addEmployee(empl);

    firma->displayEmployees();
    firma->getTotalSalary();
    return 0;
}